﻿using PockerModel_Alex_;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poker_Logic
{
    class FindWinner
    {
        private List<Client> _clientList;
        private bool _isCombinationsCounted;
        public List<Client> getListClient()
        {
            return _clientList;
        }
        public FindWinner(List<Client> _clientList)
        {
            this._clientList = _clientList;
            this._isCombinationsCounted = false;
        }

        public List<int> getIdsOfClientWithStrongestCombination()
        {
            if (_isCombinationsCounted && _clientList.Count > 0)
            {
                List<int> _winnersIds = new List<int>();
                int _maxCombinationValue = 0;
                foreach (Client client in _clientList)
                    if (client._combination._combinationValue > _maxCombinationValue)
                        _maxCombinationValue = client._combination._combinationValue;
                List<Client> _clientWithStrongestCombinationsValueList = new List<Client>();
                foreach (Client client in _clientList)
                    if (client._combination._combinationValue == _maxCombinationValue)
                        _clientWithStrongestCombinationsValueList.Add(client);

                if (_maxCombinationValue < 5 || _maxCombinationValue == 8)
                    _winnersIds = getIdsOfClientWithStrongestCard(_clientWithStrongestCombinationsValueList);
                else
                    _winnersIds = getIdsOfClientWithStrongestCardWithoutKicker(_clientWithStrongestCombinationsValueList);
                for (int i = 0; i < _clientList.Count; i++)
                    foreach (int id in _winnersIds)
                        if (_clientList[i].IdInDataBase == id)
                            _clientList.RemoveAt(i);
                return _winnersIds;
            }
            else
            {
                List<int> nothing = new List<int>();
                nothing.Add(-1);
                return nothing;
            }
        }

        private List<int> getIdsOfClientWithStrongestCardWithoutKicker(List<Client> _clientWithStrongestCombinationsValueList)
        {
            int _maxCardValue = 0;
            foreach (Client client in _clientWithStrongestCombinationsValueList)
                if (client._combination._combinationMaxCard > _maxCardValue)
                    _maxCardValue = client._combination._combinationMaxCard;
            List<int> _winners = new List<int>();
            foreach (Client client in _clientWithStrongestCombinationsValueList)
                if (client._combination._combinationMaxCard == _maxCardValue)
                    _winners.Add(client.IdInDataBase);
            return _winners;
        }

        private List<int> getIdsOfClientWithStrongestCard(List<Client> _clientWithStrongestCombinationsValueList)
        {
            int _maxCardValue = 0;
            foreach (Client client in _clientWithStrongestCombinationsValueList)
                foreach (Card i in client.Cards)
                    if (i.Points >= _maxCardValue && i.IsClientCard == true)
                        _maxCardValue = i.Points;
            List<int> _winners = new List<int>();
            bool switcher = false;
            foreach (Client client in _clientWithStrongestCombinationsValueList)
            {
                foreach (Card i in client.Cards)
                {
                    if (!switcher)
                    {
                        if (i.Points == _maxCardValue && i.IsClientCard == true)
                        {
                            _winners.Add(client.IdInDataBase);
                            switcher = true;
                        }
                    }

                }
                switcher = false;
            }
            return _winners;

        }
        //Начинаем подсчитывать комбинации пользователей
        public bool StartCheck()
        {
            //если комбина
            if (_isCombinationsCounted)
            {
                return false;
            }
            else
            {
                for (int i = 0; i < _clientList.Count; i++)
                {
                    _clientList[i]._combination = checkRoyalFlush(_clientList[i].Cards);
                }
                _isCombinationsCounted = true;
                return true;
            }
        }
        //Максимальное значение стрита(для упрощения кода)
        private int getMaxCombinationCard(List<Card> _listStraightFlush)
        {
            List<int> _maxCardValueList = new List<int>();
            foreach (Card _card in _listStraightFlush)
                _maxCardValueList.Add(_card.Points);
            foreach (Card _card in _listStraightFlush)
            {
                if (_card.Points == 2)
                    _maxCardValueList.Remove(14);
            }

            return _maxCardValueList.Max();

        }
        //Проверяем карты на одинаковость
        private bool checkCards(Card c1, Card c2)
        {
            if (c1.Face == c2.Face && c1.Points == c2.Points && c1.Suit == c2.Suit)
                return true;
            else
                return false;
        }
        private Combination checkRoyalFlush(List<Card> _cardList)
        {
            //4 листа для 4 возможных Флэш-роялей
            List<Card> _RoyalTref = new List<Card>();
            List<Card> _RoyalBubna = new List<Card>();
            List<Card> _RoyalCherv = new List<Card>();
            List<Card> _RoyalPika = new List<Card>();
            //заполнение этих листов 
            Dictionary<string, int> face = new Dictionary<string, int>() { { "Туз", 14 }, { "Десятка", 10 }, { "Валет", 11 }, { "Дама", 12 }, { "Король", 13 } };
            List<string> suit = new List<string>() { "Трефы", "Бубны", "Черви", "Пики", };
            foreach (var f in face)
                _RoyalTref.Add(new Card() { Face = f.Key, Suit = "Трефы", Points = f.Value });
            foreach (var f in face)
                _RoyalBubna.Add(new Card() { Face = f.Key, Suit = "Бубны", Points = f.Value });
            foreach (var f in face)
                _RoyalCherv.Add(new Card() { Face = f.Key, Suit = "Черви", Points = f.Value });
            foreach (var f in face)
                _RoyalPika.Add(new Card() { Face = f.Key, Suit = "Пики", Points = f.Value });
            //записать все листы в один для более легкой проверки через foreach
            List<List<Card>> _royals = new List<List<Card>>();
            _royals.Add(_RoyalBubna);
            _royals.Add(_RoyalCherv);
            _royals.Add(_RoyalPika);
            _royals.Add(_RoyalTref);
            int _temp = 0;
            foreach (List<Card> _listRoyals in _royals)
            {
                foreach (Card _cardFromRoyals in _listRoyals)
                    foreach (Card _cardFromList in _cardList)
                        if (checkCards(_cardFromList, _cardFromRoyals))
                            _temp++;
                if (_temp == 5)
                    return new Combination { _combinationMaxCard = 14, _combinationValue = 10 };
                _temp = 0;
            }
            return checkStraightFlush(_cardList);

        }
        private Combination checkStraightFlush(List<Card> _cardList)
        {
            Dictionary<int, string> face = new Dictionary<int, string>() { {  14,"Туз"}, {  2,"Двойка"}, {  3,"Тройка"}, {  4,"Четверка"}, {  5,"Пятерка"}, {  6,"Шестерка"}, {  7,"Семерка"}, {  8,"Восьмерка"}, {  9,"Девятка"}, {  10,"Десятка"}, {  11,"Валет"}, {  12,"Дама"}, {  13,"Король"}
            };
            List<string> suit = new List<string>() { "Трефы", "Бубны", "Черви", "Пики" };
            List<List<Card>> _combinations = new List<List<Card>>();
            int _temp = 0;
            //Генерируем все возвомжные комбинации стрит-флэш и паралельно проверяем комбинации с нашими картами
            foreach (string _suit in suit)
            {
                for (int i = 13; i > 5; i--)
                {
                    List<Card> _combinations2 = new List<Card>();
                    _combinations2.Add(new Card() { Face = face[i], Suit = _suit, Points = i });
                    _combinations2.Add(new Card() { Face = face[i - 1], Suit = _suit, Points = i - 1 });
                    _combinations2.Add(new Card() { Face = face[i - 2], Suit = _suit, Points = i - 2 });
                    _combinations2.Add(new Card() { Face = face[i - 3], Suit = _suit, Points = i - 3 });
                    _combinations2.Add(new Card() { Face = face[i - 4], Suit = _suit, Points = i - 4 });
                    _combinations.Add(_combinations2);
                }
                List<Card> _combinations1 = new List<Card>();
                _combinations1.Add(new Card() { Face = face[14], Suit = _suit, Points = 14 });
                _combinations1.Add(new Card() { Face = face[2], Suit = _suit, Points = 2 });
                _combinations1.Add(new Card() { Face = face[3], Suit = _suit, Points = 3 });
                _combinations1.Add(new Card() { Face = face[4], Suit = _suit, Points = 4 });
                _combinations1.Add(new Card() { Face = face[5], Suit = _suit, Points = 5 });
                _combinations.Add(_combinations1);
                foreach (List<Card> _listStraightFlush in _combinations)
                {
                    foreach (Card _cardFromSF in _listStraightFlush)
                        foreach (Card _cardFromList in _cardList)
                            if (checkCards(_cardFromList, _cardFromSF))
                                _temp++;
                    if (_temp == 5)
                    {
                        int _combinationMaxCard = getMaxCombinationCard(_listStraightFlush);
                        return new Combination { _combinationMaxCard = _combinationMaxCard, _combinationValue = 9 };
                    }
                    _temp = 0;
                }
            }
            return checkFourOfKind(_cardList);
        }
        private Combination checkFourOfKind(List<Card> _cardList)
        {
            List<Card> _cardListCopy = _cardList;
            int _temp = 0;
            //проверяем встречается ли какая-либо карта из списка 4 раза
            foreach (Card _cardCopy in _cardListCopy)
            {
                foreach (Card _card in _cardList)
                {
                    if (_card.Points == _cardCopy.Points)
                        _temp++;
                }
                Console.WriteLine();
                if (_temp == 4)
                    return new Combination { _combinationMaxCard = _cardCopy.Points, _combinationValue = 8 };
                _temp = 0;
            }
            return checkFullHouse(_cardList);
        }
        private Combination checkFullHouse(List<Card> _cardList)
        {
            //создаем лист значений карт
            List<int> _points = new List<int> { 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2 };
            int _temp = 0;
            int _max3Value = 0;
            int _max2Value = 0;
            // ищем 3 самых больших одинаковых
            foreach (int _point in _points)
            {
                foreach (Card _card in _cardList)
                {
                    if (_card.Points == _point)
                        _temp++;
                }
                if (_temp == 3)
                {
                    _max3Value = _point;
                    break;
                }
                _temp = 0;
            }
            _temp = 0;
            //удаляем значение из списка
            _points.Remove(_max3Value);
            // ищем 2 самых больших одинаковых
            foreach (int _point in _points)
            {
                foreach (Card _card in _cardList)
                {
                    if (_card.Points == _point)
                        _temp++;
                }
                if (_temp == 2)
                {
                    _max2Value = _point;
                    break;
                }
                _temp = 0;
            }
            //если хоть одна переменная 0, значит с фулхаусом не сложилось, ищем флеши
            if (_max2Value == 0 || _max3Value == 0)
                return checkFlush(_cardList);
            else
                return new Combination { _combinationValue = 7, _combinationMaxCard = Convert.ToInt32(_max3Value.ToString() + _max2Value.ToString()) };
        }
        private Combination checkFlush(List<Card> _cardList)
        {
            //лист из возможных мастей
            List<string> suit = new List<string>() { "Трефы", "Бубны", "Черви", "Пики" };
            int _temp = 0;
            string _flushSuit = "";
            //проверка встречается ли каждая из мастей >=5 раз
            foreach (string _suit in suit)
            {
                foreach (Card _card in _cardList)
                {
                    if (_card.Suit == _suit)
                        _temp++;
                }
                if (_temp >= 5)
                {
                    _flushSuit = _suit;
                    break;
                }
                _temp = 0;
            }
            int _maxValue = 0;
            //ищем максимальное значение карты с полученной мастью
            foreach (Card _card in _cardList)
                if (_card.Suit == _flushSuit)
                    if (_card.Points >= _maxValue)
                        _maxValue = _card.Points;
            //если совпадения есть отправляем комбинации
            if (_flushSuit != "" && _maxValue != 0)
                return new Combination { _combinationMaxCard = _maxValue, _combinationValue = 6 };
            //нет? ну тогда ищем дальше
            else
                return checkStraight(_cardList);
        }
        private Combination checkStraight(List<Card> _cardList)
        {
            //добавляем возможные стриты по убыванию их силы
            int _temp = 0;
            List<List<Card>> _combinations = new List<List<Card>>();
            for (int i = 14; i > 5; i--)
            {
                List<Card> _combinations2 = new List<Card>();
                _combinations2.Add(new Card { Points = i });
                _combinations2.Add(new Card { Points = i - 1 });
                _combinations2.Add(new Card { Points = i - 2 });
                _combinations2.Add(new Card { Points = i - 3 });
                _combinations2.Add(new Card { Points = i - 4 });
                _combinations.Add(_combinations2);
            }
            List<Card> _combinations1 = new List<Card>();
            _combinations1.Add(new Card { Points = 14 });
            _combinations1.Add(new Card { Points = 2 });
            _combinations1.Add(new Card { Points = 3 });
            _combinations1.Add(new Card { Points = 4 });
            _combinations1.Add(new Card { Points = 5 });
            _combinations.Add(_combinations1);
            List<Card> _tempList = new List<Card>();
            //проверяем на наши карты на совпадения с сгенерированным листом
            foreach (List<Card> _listStraight in _combinations)
            {
                foreach (Card _cardFromStraight in _listStraight)
                    _tempList.Add(new Card { Face = _cardFromStraight.Face, Points = _cardFromStraight.Points, Suit = _cardFromStraight.Suit });
                //при совпадении карты из сгенеривованного последовательности заменяем ее значение на -1 для избежания повторения одинаковых карт
                for (int i = 0; i < 5; i++)
                {
                    foreach (Card _cardFromList in _cardList)
                        if (_listStraight[i].Points == _cardFromList.Points)
                        {
                            _temp++;
                            _listStraight[i].Points = -1;
                        }

                    //нашли совпадение - отправили комбинацию на вычисление максимальной карты и получаем комбинацию


                }
                if (_temp == 5)
                {
                    int _combinationMaxCard = getMaxCombinationCard(_tempList);
                    return new Combination { _combinationMaxCard = _combinationMaxCard, _combinationValue = 5 };
                }
                _temp = 0;
                _tempList.Clear();


            }
            //если не нашлось совпадений идем дальше
            return checkSet(_cardList);
        }
        private Combination checkSet(List<Card> _cardList)
        {
            //создаем лист значений карт
            List<int> _points = new List<int> { 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2 };
            int _temp = 0;
            int _max3Value = 0;
            // ищем 3 самых больших одинаковых
            foreach (int _point in _points)
            {
                foreach (Card _card in _cardList)
                {
                    if (_card.Points == _point)
                        _temp++;
                }
                if (_temp == 3)
                {
                    _max3Value = _point;
                    break;
                }
                _temp = 0;
            }

            //если хоть одна переменная 0, значит с сетом не сложилось, ищем две пары
            if (_max3Value == 0)
                return checkDoublePair(_cardList);
            else
                return new Combination { _combinationValue = 4, _combinationMaxCard = _max3Value };
        }
        private Combination checkDoublePair(List<Card> _cardList)
        {
            //создаем лист значений карт
            List<int> _points = new List<int> { 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2 };
            int _temp = 0;
            int _max2Value1 = 0;
            int _max2Value2 = 0;
            // ищем 3 самых больших одинаковых
            foreach (int _point in _points)
            {
                foreach (Card _card in _cardList)
                {
                    if (_card.Points == _point)
                        _temp++;
                }
                if (_temp == 2)
                {
                    _max2Value1 = _point;
                    break;
                }
                _temp = 0;
            }
            _temp = 0;
            //удаляем значение из списка
            _points.Remove(_max2Value1);
            // ищем 2 самых больших одинаковых
            foreach (int _point in _points)
            {
                foreach (Card _card in _cardList)
                {
                    if (_card.Points == _point)
                        _temp++;
                }
                if (_temp == 2)
                {
                    _max2Value2 = _point;
                    break;
                }
                _temp = 0;
            }
            //если хоть одна переменная 0, значит с двумя парами не сложилось, ищем пару
            if (_max2Value1 == 0 || _max2Value2 == 0)
                return checkPair(_cardList);
            else
                return new Combination { _combinationValue = 3, _combinationMaxCard = Convert.ToInt32(_max2Value1.ToString() + _max2Value2.ToString()) };
        }
        private Combination checkPair(List<Card> _cardList)
        {
            //создаем лист значений карт
            List<int> _points = new List<int> { 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2 };
            int _temp = 0;
            int _max2Value = 0;
            foreach (int _point in _points)
            {
                foreach (Card _card in _cardList)
                {
                    if (_card.Points == _point)
                        _temp++;
                }
                if (_temp == 2)
                {
                    _max2Value = _point;
                    break;
                }
                _temp = 0;
            }

            //если хоть одна переменная 0, значит с парой не сложилось, ищем кикер
            if (_max2Value == 0)
                return checkKicker(_cardList);
            else
                return new Combination { _combinationValue = 2, _combinationMaxCard = _max2Value };
        }
        private Combination checkKicker(List<Card> _cardList)
        {
            //создаем лист значений карт
            //List<int> _points = new List<int> { 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2 };
            List<int> _points = new List<int> { 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 };
            int _kicker1 = 0;
            int _kicker2 = 0;
            // ищем 3 самых больших одинаковых
            foreach (int _point in _points)
            {
                foreach (Card _card in _cardList)
                {
                    if (_card.Points == _point)
                    {
                        _kicker1 = _point;
                        break;
                    }
                }
            }
            //удаляем значение из списка
            _points.Remove(_kicker1);
            // ищем 2 самых больших одинаковых
            foreach (int _point in _points)
            {
                foreach (Card _card in _cardList)
                {
                    if (_card.Points == _point)
                    {
                        _kicker2 = _point;
                        break;
                    }
                }
            }
            //если хоть одна переменная 0, значит с парой не сложилось, ищем флеши
            if (_kicker1 == 0 || _kicker2 == 0)
                return new Combination { _combinationValue = 0, _combinationMaxCard = 0 };
            else
                return new Combination { _combinationValue = 1, _combinationMaxCard = Convert.ToInt32(_kicker1.ToString() + _kicker2.ToString()) };
        }
    }
}
