﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PockerModel_Alex_;

namespace Poker_Logic
{
    class Bank
    {
        private List<List<Bet>> _betList;
        private Dictionary<int, int> _idAndMaxBankToWin;
        private int _globalBank;

        public Bank(List<List<Bet>> BetList)
        {
            _idAndMaxBankToWin = new Dictionary<int, int>();
            _betList = BetList;
            int _temp = 0;
            foreach (List<Bet> _listBet in _betList)
            {
                foreach (Bet _bet in _listBet)
                {
                    _temp += _bet.BetValue;
                }
            }
            _globalBank = _temp;
            setDictionary();
        }

        private void setDictionary()
        {
            foreach (List<Bet> _listBet in _betList)
            {
                foreach (Bet _bet in _listBet)
                {
                    if (_bet.IsAllIn == true)
                    {
                        int _tmp = 0;
                        foreach (List<Bet> _listBet1 in _betList)
                        {
                            foreach (Bet _bet1 in _listBet1)
                            {
                                if (_bet1.BetOrder < _bet.BetOrder)
                                    _tmp += _bet1.BetValue;
                                else
                                {
                                    if (_bet1.BetOrder == _bet.BetOrder)
                                    {
                                        if (_bet1.BetValue - _bet.BetValue < 0)
                                            _tmp += _bet1.BetValue;
                                        else
                                            _tmp += _bet.BetValue;
                                    }
                                }
                            }
                        }
                        _idAndMaxBankToWin.Add(_bet.ClientId, _tmp);
                        _tmp = 0;
                    }
                }
            }
            foreach (List<Bet> _listBet in _betList)
            {
                foreach (Bet _bet in _listBet)
                {
                    if(!_idAndMaxBankToWin.Keys.Contains(_bet.ClientId))
                    {
                        _idAndMaxBankToWin.Add(_bet.ClientId, _globalBank);
                    }
                }
            }

        }
        public Dictionary<int,int> getCountedDictionary()
        {
            return _idAndMaxBankToWin;
        }
        public int getGlobalBank()
        {
            return _globalBank;
        }
    }
}
