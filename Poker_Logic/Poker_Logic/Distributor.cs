﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poker_Logic
{
    class Distributor
    {
        private int _bank;
        private Dictionary<int, int> _maxBanks;
        private Dictionary<int, int> _maxBanksCopy;
        private List<List<int>> _winnersIds;
        private Dictionary<int, int> _idAndWin;

        public Distributor(int bank, Dictionary<int,int> maxBanks, List<List<int>> winnersIds)
        {
            _maxBanksCopy = maxBanks;
            _bank = bank;
            _maxBanks = maxBanks;
            _winnersIds = winnersIds;
            _idAndWin = new Dictionary<int, int>();
        }
        public void Distribute()
        {
            foreach (List<int> _list in _winnersIds)
            {
                if (_list.Count == 1)
                {
                    foreach (KeyValuePair<int, int> i in _maxBanks)
                    {
                        if (_list[0] == i.Key)
                        {
                            if (i.Value < _bank)
                            {
                                _idAndWin.Add(i.Key, i.Value);
                                _bank -= i.Value;
                            }
                            else
                            {
                                _idAndWin.Add(i.Key, _bank);
                                _bank = 0;
                            }
                        }
                    }
                }
                else
                {
                    do
                    {
                        int part = _bank / _list.Count();
                        foreach (int ddd in _list)
                        {
                            foreach (KeyValuePair<int, int> i in _maxBanks)
                            {
                                if (ddd == i.Key)
                                {
                                    if (i.Value < part)
                                    {
                                        _idAndWin.Add(i.Key, i.Value);
                                        _bank -= i.Value;
                                        _bank += (part - i.Value);
                                    }
                                    else
                                    {
                                        if (_idAndWin.ContainsKey(i.Key))
                                        {
                                            if ((_idAndWin[i.Key] + part < i.Value))
                                            {
                                                _idAndWin[i.Key] += part;
                                                _bank -= part;
                                            }
                                        }
                                        else
                                        {
                                            _idAndWin.Add(i.Key, part);
                                            _bank -= part;
                                        }
                                    }
                                }
                            }
                        }
                    } while (_bank / _list.Count() >= 1);
                    if (_bank == 1)
                        _bank = 0;
                }
            }
        }
        public Dictionary<int,int> getMoney()
        {
                    foreach(int i in _maxBanksCopy.Keys)
                        if(!_idAndWin.Keys.Contains(i))
                            _idAndWin.Add(i, 0);
                return _idAndWin;
        }
    }
}
