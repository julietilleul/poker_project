﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace PockerModel_Alex_
{
    [System.Serializable]
    public class Card
    {
        public bool IsClientCard { get; set; }

        public System.Drawing.Bitmap pic = PockerModel_Alex_.Properties.Resources.cards;

        public string Face { get; set; }//что за карта
        public string Suit { get; set; }//масть
        public int Points { get; set; }//количество очков карты
        public int BeginX { get; set; }//начало координат картинки по Х
        //public int EndX { get { return BeginX + 148; } }//конец координат картинки по Х
        public int BeginY { get; set; }//начало координат картинки по У
        //public int EndY { get { return BeginY + 215; } }//конец координат картинки по У
        public Card()
        {
            SettingCard();

        }
        public System.Windows.Controls.Image GetControlsImageCard
        {
            get
            {
                 System.Drawing.Image image2 = pic.Clone(new System.Drawing.Rectangle(BeginX, BeginY, 148, 215), System.Drawing.Imaging.PixelFormat.Format16bppRgb555);
                using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
                {
                    image2.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                    return new System.Windows.Controls.Image() { Width = width, Height = height, Source = GetBitmapImage(ms.ToArray()) };
                }
            }
        }
        ////////////////////////////////////////////////////////////////////////////
        int sizeCard = 100;
        double coef;
        int width;
        int height;
        private void SettingCard()
        {
            
            coef = 148.0 / 215.0;
            width = (int)(sizeCard * coef);
            height = sizeCard;

        }
        private BitmapImage GetBitmapImage(byte[] imageBytes)
        {
            BitmapImage bitmapImage = new BitmapImage();
            bitmapImage.BeginInit();
            bitmapImage.StreamSource = new System.IO.MemoryStream(imageBytes);
            bitmapImage.EndInit();
            return bitmapImage;
        }
    }
}
