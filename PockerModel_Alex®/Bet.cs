﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PockerModel_Alex_
{
    public class Bet
    {
        public int BetValue { get; set; }
        public int ClientId { get; set; }
        public bool IsAllIn { get; set; }
        public int BetOrder { get; set; }

        public Bet()
        {
            IsAllIn = false;
        }
    }
}
