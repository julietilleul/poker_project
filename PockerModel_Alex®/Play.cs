﻿using System;
using System.Collections.Generic;
using System.Linq;
using Basic;
using PockerModel_Alex_;

namespace Base
{
    //public class Client
    //{
    //    public bool Dealer { get; set; }
    //    /// <summary>
    //    /// Игрок на котором заканчивается ход
    //    /// </summary>
    //    public bool endPlayer { get; set; }
    //}



    public class Play
    {
        Deck deckT;
        /// <summary>
        /// Игроки, участвюие в текущем розыгрыше
        /// </summary>
        List<Client> playingClients;
        /// <summary>
        /// Игрок на котором заканчивается текущий ход
        /// </summary>
        int endPlayer;
        int oldMaxBet = 0;
        int turn;


        int curBet;
        int Bank;

        #region GameStart
        public Play(List<Client> cl, Deck d)
        {
            playingClients = cl;
            deckT = d;
            InitialPreparation();
            turn = 0;
        }
        public void InitialPreparation()
        {

            ChooseDealer(playingClients);
            GetStartCards(playingClients, deckT);
        }

        /// <summary>
        /// Назначает дилера
        /// </summary>
        /// <param name="pl">текущие игроки за столом</param>
        public void ChooseDealer(List<Client> pl)
        {
            if (pl.Any(x => x.Dealer) == null)
            {
                Random r = new Random();
                pl[r.Next(0, pl.Count)].Dealer = true;
            }
            else
            {
                int i = pl.FindIndex(cl => cl.Dealer == true);
                pl[i].Dealer = false;

                if (i + 1 == pl.Count)
                    pl[0].Dealer = true;
                else
                    pl[i + 1].Dealer = true;
            }
        }
        public void GetStartCards(List<Client> pl, Deck d)
        {
            for (int i = 0; i < 2; i++)
            {
                foreach (Client cl in pl)
                {
                    cl.Cards.Add(d.GetCard);
                    cl.st = Client.Status.ACTIVES;
                }
            }

        }
        /// <summary>
        /// Выбирает игрока на котором заканчивается ход - изначальный
        /// </summary>
        /// <param name="pl">список игроков</param>
        /// <returns>номер игрока</returns>
        public void ChooseEndPlayer(List<Client> pl)
        {
            int i = pl.FindIndex(cl => cl.Dealer == true);

            //Если дилер - граничная величина
            if (i + 1 == pl.Count)
                /* return 1;*/
                pl[1].endPlayer = true;
            if (i + 2 == pl.Count)
                /*return 0;*/
                pl[0].endPlayer = true;
            /*return i + 2;*/
            pl[i + 2].endPlayer = true;
        }

        /// <summary>
        /// Игроки делают 1 ставку
        /// </summary>
        /// <param name="pl">все игроки</param>
        /// <param name="cBet">текущая ставка</param>
        /// <returns>деньги в банке</returns>
        public int MakeFirstBet(List<Client> pl, int cBet)
        {

            int i = pl.FindIndex(cl => cl.Dealer == true);
            int bank = 0;


            if (i + 1 == pl.Count)
            {
                bank = MakeBet(pl[0], cBet / 2);
                bank = MakeBet(pl[1], cBet);
            }

            else
                if (i + 2 == pl.Count)
                {
                    bank = MakeBet(pl[i + 1], cBet / 2);
                    bank = MakeBet(pl[0], cBet);
                }
                else
                {
                    bank = MakeBet(pl[i + 1], cBet / 2);
                    bank = MakeBet(pl[i + 2], cBet);
                }
            return bank;

        }
        #endregion


        public void PreFlop()
        {
            deckT.CreateDeck(); 
            InitialPreparation();
            turn=1;
        }
        public void Flop(List<Card> card)
        {
            FlopCardsRecieve(card);
            turn++;
        }

        public void River(List<Card> card)
        {
            RiverCardsRecieve(card);
            turn++;
        }

        /// <summary>
        /// Выбирает игрока на котором заканчивается ход - повышение ставки
        /// </summary>
        /// <param name="pl">список игроков</param>
        /// <param name="cPlayer">повысивший игрок</param>
        /// <returns>номер игрока</returns>
        public void ChooseEndPlayer(List<Client> plList, int cPlayer)
        {
            foreach (var pl in plList)
            {
                pl.endPlayer = false;
            }
            plList[cPlayer].endPlayer = true;
        }

        #region PlayersActions
        /// <summary>
        /// Игрок делает ставку
        /// </summary>
        /// <param name="pl">Игрок</param>
        /// <param name="curBet">Общая ставка</param>
        /// <returns>Разница тек-пред ставки</returns>
        public int MakeBet(Client pl, int curBet)
        {
            if (pl.BetC.Any())
            {
                int i = curBet - pl.BetC.LastOrDefault().BetValue;
                pl.Money -= i;
                pl.BetC.Add(new Bet(){BetValue=i, ClientId = pl.IdInDataBase});
                return i;
            }
            return 0;
        }
        public int Raise(List<Client> plList, Client pl, int playersBet, ref int oldMaxBet)
        {
            int i = MakeBet(pl, playersBet);
            oldMaxBet += i;
            ChooseEndPlayer(plList, plList.FindIndex(cl => cl == pl));
            return i;
        }

        public int ALLIN(List<Client> plList, Client pl, int playersBet, ref int oldMaxBet)
        {
            pl.st = Client.Status.ALLIN;

            if (playersBet < oldMaxBet)
            {
                int i = 0;
                i = Raise(plList, pl, playersBet, ref i);
                pl.BetC.LastOrDefault().IsAllIn = true;
                return i / 2;
            }
            else
            {
                int i = Raise(plList, pl, playersBet, ref oldMaxBet);
                pl.BetC.LastOrDefault().IsAllIn = true;
                return i;
            }
        }

        /// <summary>
        /// Игрок скидывает карты
        /// </summary>
        /// <param name="pl">игрок,произвевший действие</param>
        public void Pass(Client pl)
        {
            pl.st = Client.Status.PASIVE;
            
        }

        #endregion

        #region NewTurns
        /// <summary>
        /// Ложит одну карту на стол и добавляет к коллекции карт игроков
        /// </summary>
        /// <param name="pl">Все игроки</param>
        /// <param name="d">колода банка</param>
        /// <param name="tableCards">карты на столе</param>
        public void RiverCardsRecieve(List<Card> tableCards)
        {
            tableCards.Add(deckT.GetCard);
            foreach (Client c in playingClients)
            {
                if (c.st == Client.Status.ACTIVES)
                {
                    c.Cards.Add(tableCards[tableCards.Count - 1]);
                }
            }
        }
        /// <summary>
        /// Ложит 3 карты на стол и добавляет к коллекции карт игроков
        /// </summary>
        /// <param name="pl">Все игроки</param>
        /// <param name="d">колода банка</param>
        /// <param name="tableCards">карты на столе</param>
        public void FlopCardsRecieve(List<Card> tableCards)
        {
            for (int i = 0; i < 3; i++)
            {
                RiverCardsRecieve(tableCards);
            }
            
        }
        #endregion

        /// <summary>
        /// мин ставка для Raise
        /// </summary>
        /// <param name="pl">Текущий игрок</param>
        /// <param name="curBet">Текущая макс ставка</param>
        /// <returns>Возможная мин ставка</returns>
        public int CalculateMinAddBet(Client pl, int curBet)
        {
            if (pl.Money + pl.BetC.LastOrDefault().BetValue < curBet * 2)
            {
                return pl.Money + pl.BetC.LastOrDefault().BetValue;
            }
            else
            {
                return curBet * 2;
            }
        }

        public int NextPlayer(List<Client> plList, Client curPl)
        {
            int j = plList.IndexOf(curPl);
            for (int i = j + 1; i < plList.Count; i++)
            {
                if (plList[i].st == Client.Status.ACTIVES)
                    return i;
                if (plList[i] == curPl)
                {
                    return i;
                }
                if (i + 1 == plList.Count)
                    i = -1;
            }
            return 0;
        }

        public bool CheckAllIn(List<Client> plList)
        {
            foreach (var item in plList)
            {
                if (item.st == Client.Status.ALLIN)
                    return true;
            }
            return false;
        }
        public bool AllPlAreAllin(List<Client> plList)
        {
            int i = 0;
            foreach (var item in plList)
            {
                if (item.st == Client.Status.ALLIN)
                    i++;
            }
            return i == plList.Count ? true : false;
        }
    }
}
