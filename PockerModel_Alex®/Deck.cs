﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PockerModel_Alex_
{
    [System.Serializable]
    public class Deck
    {
        //колода карт
        public System.Collections.Generic.List<Card> deck { get; set; }
        Dictionary<string, int[]> face;
        Dictionary<string, int[]> suit;
        public Card GetCard//отдаем одну карту из колоды и удалеме ее же из колоды
        {
            get
            {
                Card c = deck.Last<Card>();
                deck.Remove(c);
                return c;
            }
        }

        public Deck()
        {
            deck = new System.Collections.Generic.List<Card>();
            face = new Dictionary<string, int[]>() { 
                { "Туз", new int[3]{11, 0, 5}}, 
                { "Двойка", new int[3]{2, 147, 5} }, 
                { "Тройка", new int[3]{3, 295, 5} }, 
                { "Четверка", new int[3]{4, 442, 5} }, 
                { "Пятерка", new int[3]{5, 590, 5} }, 
                { "Шестерка", new int[3]{6, 738, 5} }, 
                { "Семерка", new int[3]{7, 885, 5} }, 
                { "Восьмерка", new int[3]{8, 1033, 5} }, 
                { "Девятка", new int[3]{9, 1181, 5} }, 
                { "Десятка", new int[3]{10, 1328, 5} }, 
                { "Валет", new int[3]{10, 1476, 5} }, 
                { "Дама", new int[3]{10, 1624, 5} }, 
                { "Король", new int[3]{10, 1771, 5} }
            };

            suit = new Dictionary<string, int[]>()
            {
                { "Трефы", new int[2]{0, 0}}, 
                { "Бубны", new int[2]{214, 0}}, 
                { "Черви", new int[2]{429, 0}}, 
                { "Пики", new int[2]{643, 0}}, 
            };


            CreateDeck();
        }
        public void CreateDeck()
        {
            foreach (var s in suit)
                foreach (var f in face)
                    deck.Add(new Card() { Face = f.Key, Suit = s.Key, Points = f.Value[0], BeginX = f.Value[1], BeginY = s.Value[0] });

            MixDeck();
        }
        private void MixDeck()//перемешивание колоды
        {
            System.Random rand1 = new System.Random(System.DateTime.Now.Millisecond);
            System.Threading.Thread.Sleep(50);
            System.Random rand2 = new System.Random(System.DateTime.Now.Millisecond);
            int numb1 = 0;
            int numb2 = 0;

            Card card = new Card();

            for (int i = 0; i < deck.Count / 2; i++)
            {
                numb1 = rand1.Next(deck.Count);
                numb2 = rand2.Next(deck.Count);
                card = deck[numb1];
                deck[numb1] = deck[numb2];
                deck[numb2] = card;
            }
        }

    }
}
