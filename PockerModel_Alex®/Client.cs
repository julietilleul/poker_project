﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PockerModel_Alex_
{
    [System.Serializable]
    public class Client
    {
        // add combination for client
        public Combination _combination { get; set; }
        public int IdInDataBase { get; set; }

        public enum Status { ACTIVES, PASIVE, ALLIN }
        public bool Dealer { get; set; }

        public List<Image> MyCards { get; set; }
        /// <summary>
        /// Игрок на котором заканчивается ход
        /// </summary>
        public bool endPlayer { get; set; }
        public bool playerTurn { get; set; } //ход этого игрока

        public Status st = Status.PASIVE;
        public List<Card> Cards { get; set; }//карты получиные клиентом
        public List<Bet> BetC { get; set; }
        public int Money { get; set; }
        System.Drawing.Bitmap avatarca;//аватарка

        public string Name { get; private set; }//имя клиента
        public int NumberInTable { get; private set; }//номер за столом

        public Client(string name, int number, string avaFileName = "Smile")
        {
            Cards = new List<Card>();
            this.avatarca = new System.Drawing.Bitmap(avaFileName);
            this.Name = name;
            this.NumberInTable = number;
            MyCards = new List<Image>();
            MyCards.Add(new Bitmap("images (1).png"));
            MyCards.Add(new Bitmap("images (2).png"));
            BetC = new List<Bet>();
        }
    }
}
