﻿using PockerModel_Alex_;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Basic
{
    [System.Serializable]
    public class Table : INotifyPropertyChanged
    {
        private List<Client> clientList = new List<Client>();
        private Deck deck = new Deck();
        public List<Card> tableList=new List<Card>();
        private int bank = 0;
        private int bet = 0;

        #region Properties with INot
        public List<Client> ClientList
        {
            get { return clientList; }
            set
            {
                if (value == clientList)
                    return;
                clientList = value;
                OnPropertyChanged("ClientList");
            }
        }
        public Deck Deck
        {
            get { return deck; }
            set
            {
                if (value == deck)
                    return;
                deck = value;
                OnPropertyChanged("Deck");
            }
        }
        public int Bank
        {
            get { return bank; }
            set
            {
                if (value == bank)
                    return;
                bank = value;
                OnPropertyChanged("Bank");
            }
        }
        public int Bet
        {
            get { return bet; }
            set
            {
                if (value == bet)
                    return;
                bet = value;
                OnPropertyChanged("Bet");
            }
        }
        #endregion

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
