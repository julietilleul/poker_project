﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace PokerService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IPokerGameService" in both code and config file together.
    [ServiceContract(CallbackContract = typeof(IReportServiceCallBack))] //!!!!!!!!!!!!привязка интерфейса калбека
    public interface IPokerGameService
    {
        [OperationContract(IsOneWay = true)] //DUPLEX
        void ConnectToService();
    }
    public interface IReportServiceCallBack
    {
        [OperationContract]
        void GetNumberAtTheTable(int x);
    }
}
