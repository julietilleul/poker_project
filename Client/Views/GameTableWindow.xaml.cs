﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
//using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Client.ViewModels;
using Client.Abstract;
using System.Threading;
using Base;
using Basic;

namespace Client.Views
{
    /// <summary>
    /// Interaction logic for GameTableWindow.xaml
    /// </summary>
    public partial class GameTableWindow : Window
    {
        int turn = 0;
        Table table = new Table();
        Play p;
        public GameTableWindow()
        {
            InitializeComponent();
            p = new Play(table.ClientList, table.Deck);

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (turn<3)
            {
                BidImitation();  
            }
           
            if (turn == 2)
            {
                turn = 0;
                IMAGE5.Visibility = Visibility.Visible;
                WinnerImitation();
                
            }
            else
            if (turn == 1)
            {
                turn++;
                IMAGE4.Visibility = Visibility.Visible;
            }
            else
            if (turn == 0)
            {
                turn++;
                IMAGE1.Visibility = Visibility.Visible;
                IMAGE2.Visibility = Visibility.Visible;
                IMAGE3.Visibility = Visibility.Visible;
            }
           
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            BidImitation();
            WinnerImitation();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            WinnerImitation();
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            if (turn < 3)
            {
                BidImitation();
            }

            if (turn == 2)
            {
                turn = 0;
                IMAGE5.Visibility = Visibility.Visible;
                WinnerImitation();

            }
            else
                if (turn == 1)
                {
                    turn++;
                    IMAGE4.Visibility = Visibility.Visible;
                }
                else
                    if (turn == 0)
                    {
                        turn++;
                        IMAGE1.Visibility = Visibility.Visible;
                        IMAGE2.Visibility = Visibility.Visible;
                        IMAGE3.Visibility = Visibility.Visible;
                    }
        }

        void BidImitation() 
        {
            Random r=new Random();
            if (Convert.ToInt32(MyMoney.Text) < Convert.ToInt32(Label2.Content))
                Label2.Content = MyMoney.Text;
            else
            {
                Label2.Content = Convert.ToInt32(Label2.Content) + r.Next(1, 15);
                MyMoney.Text = Label2.Content.ToString();
            }
            Label3.Content = Label2.Content;
            Label4.Content = Label2.Content;

            AllMoney.Content = Convert.ToInt32(MyMoney.Text) + Convert.ToInt32(Label2.Content) + Convert.ToInt32(Label3.Content) + Convert.ToInt32(Label4.Content);
        }

        void WinnerImitation()
        {
            Random r = new Random();
            int i = r.Next(1, 4);
            switch (i)
            {
                case 1:
                    MessageBox.Show("You Win!");
                    break;
                case 2:
                    MessageBox.Show("player 2 win");
                    break;
                case 3:
                    MessageBox.Show("player 3 win");
                    break;
                case 4:
                    MessageBox.Show("player 4 win");
                    break;
                default:
                    break;
            }

            IMAGE1.Visibility = Visibility.Hidden;
            IMAGE2.Visibility = Visibility.Hidden;
            IMAGE3.Visibility = Visibility.Hidden;
            IMAGE4.Visibility = Visibility.Hidden;
            IMAGE5.Visibility = Visibility.Hidden;
            AllMoney.Content = 0;
            MyMoney.Text = "0";
            Label2.Content = 0; 
            Label3.Content= 0;
            Label4.Content= 0;
        }
    }
}
