﻿using Basic;
using Client.Abstract;
using Client.Helpers;
using PockerModel_Alex_;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Client.ViewModels
{
    class TableGameViewModel : BaseViewModel
    {
        #region Prop
        /// <summary>
        /// Номер моего игрока за столом
        /// </summary>
        int myTableNumber = 0;
        /// <summary>
        /// Для поля со ставкой для повышения
        /// </summary>
        int turn=0;

        public int MyPlayersBet
        {
            get
            {
                if (MyPlayer.Bet < MainTable.Bet)
                    MyPlayer.Bet = MainTable.Bet;
                return MyPlayer.Bet;
            }
            set
            {
                if (MyPlayer.Bet != value)
                    MyPlayer.Bet = value;
                OnPropertyChanged("MyPlayersBet");
            }
        }

        private List<Card> tableCards;

        public List<Card> TableCards
        {
            get { return MainTable.tableCards; }
            set { MainTable.tableCards = value; }
        }

        private Table mainTable;
        public Table MainTable
        {
            get { return mainTable; }
            set
            {
                if (mainTable != value)
                    mainTable = value;
                OnPropertyChanged("MainTable");
            }
        }


        /// <summary>
        /// Мой игрок
        /// </summary>
        public PockerModel_Alex_.Client MyPlayer
        {
            get
            {
                return MainTable.ClientList.Where(x => x.NumberInTable == myTableNumber).FirstOrDefault();
            }
            set
            {
                PockerModel_Alex_.Client cl = MainTable.ClientList.Where(x => x.NumberInTable == myTableNumber).FirstOrDefault();
                cl = value;
                OnPropertyChanged("MyPlayer");
            }
        }
        #region Players
        public PockerModel_Alex_.Client Player1
        {
            get
            {
                if (MainTable.ClientList[0].NumberInTable != myTableNumber)
                    return MainTable.ClientList[0];
                else
                    return MainTable.ClientList[1];
            }
            set
            {
                if (MainTable.ClientList[0].NumberInTable != myTableNumber)
                    MainTable.ClientList[0] = value;
                else
                    MainTable.ClientList[1] = value;
                OnPropertyChanged("Player1");
            }
        }
        public PockerModel_Alex_.Client Player2
        {
            get
            {
                if (MainTable.ClientList[1].NumberInTable != myTableNumber)
                    return MainTable.ClientList[1];
                else
                    return MainTable.ClientList[2];
            }
            set
            {
                if (MainTable.ClientList[1].NumberInTable != myTableNumber)
                    MainTable.ClientList[1] = value;
                else
                    MainTable.ClientList[2] = value;
                OnPropertyChanged("Player2");
            }
        }
        public PockerModel_Alex_.Client Player3
        {
            get
            {
                if (MainTable.ClientList[2].NumberInTable != myTableNumber)
                    return MainTable.ClientList[2];
                else
                    return MainTable.ClientList[3];
            }
            set
            {
                if (MainTable.ClientList[2].NumberInTable != myTableNumber)
                    MainTable.ClientList[2] = value;
                else
                    MainTable.ClientList[3] = value;

                OnPropertyChanged("Player3");
            }
        }
        #endregion
        #region Commands
        public ICommand RaiseCommand { get; set; }
        public ICommand CheckCommand { get; set; }
        public ICommand PassCommand { get; set; }
        public ICommand AllInCommand { get; set; }
        #endregion

        ITranferer transf;
        #endregion

        public TableGameViewModel(Table t, int myPlayerTableNum)
        {
            MainTable = t;
            myTableNumber = myPlayerTableNum;
            CommandAnnounce();

        }

        void CommandAnnounce()
        {
            RaiseCommand = new RelayCommand(arg => RaiseMethod());
            CheckCommand = new RelayCommand(arg => CheckMethod());
            PassCommand = new RelayCommand(arg => PassMethod());
            AllInCommand = new RelayCommand(arg => AllInMethod());
        }
        #region Methods

        void RaiseMethod()
        {
            MyPlayer.Bet = Convert.ToInt32(MyPlayersBet);
            MainTable.Bet = MyPlayer.Bet;
            //transf.SendTable(MainTable);
        }
        void CheckMethod()
        {
            
        }
        void PassMethod()
        {
            MyPlayer.st = PockerModel_Alex_.Client.Status.PASIVE;
            // transf.SendTable(MainTable);
        }
        void AllInMethod()
        {
            MyPlayer.Bet = MyPlayer.Money;
            MainTable.Bet = MyPlayer.Bet;
            MyPlayer.st = PockerModel_Alex_.Client.Status.ALLIN;
            //transf.SendTable(MainTable);
        }
        #endregion
    }
}
