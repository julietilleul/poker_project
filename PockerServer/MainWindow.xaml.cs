﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PockerServer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Controller _controller = new Controller();
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btStartService_Click(object sender, RoutedEventArgs e)
        {
            lbEnents.Items.Add(DateTime.Now.ToLongTimeString() + " Starting service...");
            try
            {
                if (_controller.ServiceStart())
                {
                    lbServiceStatus.Content = "Server working";
                    lbServiceStatus.Background = Brushes.Green;
                    btStartService.IsEnabled = false;
                    btStopService.IsEnabled = true;
                    lbEnents.Items.Add(DateTime.Now.ToLongTimeString() + " Starting service successful");
                    return;
                }
                lbEnents.Items.Add(DateTime.Now.ToLongTimeString() + " Starting is working!!!");
            }
            catch (Exception)
            {
                lbEnents.Items.Add(DateTime.Now.ToLongTimeString() + " Starting server failed!");
            }
        }

        private void btStopService_Click(object sender, RoutedEventArgs e)
        {
            lbEnents.Items.Add(DateTime.Now.ToLongTimeString() + " Try stop server...");
            try
            {
                if (_controller.ServiceStop())
                {
                    lbServiceStatus.Content = "Server stoped";
                    lbServiceStatus.Background = Brushes.Red;
                    btStartService.IsEnabled = true;
                    btStopService.IsEnabled = false;
                    lbEnents.Items.Add(DateTime.Now.ToLongTimeString() + " Server stop successful");
                    return;
                }
                lbEnents.Items.Add(DateTime.Now.ToLongTimeString() + " Server not run!");
            }
            catch (Exception)
            {
                lbEnents.Items.Add(DateTime.Now.ToLongTimeString() + " Server stop failed!");
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                _controller.ServiceStop();
            }
            catch (Exception)
            {
                MessageBox.Show("Server stop failed!", "Exeption", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
        }
    }
}
