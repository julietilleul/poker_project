﻿using Basic;
using PockerModel_Alex_;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace PockerServer
{
    public class Controller
    {
        public Table table; //стол контроллера
        ServiceHost host;
        PockerService service;
        public Controller()
        {
            
        }
        //Методы для работы с базой данных которой еще нет)
        public bool CheckUserInBase(string name, int pass)
        {
            //проверка логина и пароля в базе
            return true;
        }
        public bool AddNewUserInBase(string name, int pass)
        {
            //здесь ОБЯЗАТЕЛЬНО нужно проверить что такого пользователя еще нет (по логину)
            return true;
        }

        //Методы для работы с списком склиетов
        public bool AddClientInList(string name)
        {
            if (table.ClientList.Count > 7)
                return false;
            for (int i = 1; i < 9; i++)
            {
                bool flag = false;
                foreach (Client item in table.ClientList)
                {
                    if (item.NumberInTable == i)
                        continue;
                    flag = true;                    
                }
                if(flag)
                {
                    table.ClientList.Add(new Client(name, i));
                    return true;
                }
            }            
            return false;
        }
        public bool RemoveClientFromList(string name)
        {
            var query = from c in table.ClientList where c.Name == name select c;
            try
            {
                if(table.ClientList.Remove(query.First()))
                    return true;
                return false;
            }
            catch (Exception)
            {
                return false;
            }            
        }

        public void TakeTable(string name, Table table)//ползователь прислал новый стол
        {
            for (int i = 1; i < 9; i++)//перевод хода
            {
                if (table.ClientList[i].Name == name)
                {
                    table.ClientList[i].playerTurn = false;//делаем клиента пассивным
                    if (i >= table.ClientList.Count())
                        i = 0;
                    i++;
                    table.ClientList[i].playerTurn = true;//включаем следующего
                    break;
                }
            }
            this.table = table;
        }

        //запук/остановка сервиса
        public bool ServiceStart()
        {
            try
            {
                 table = new Table();
            service = new PockerService(this);
            host = new ServiceHost(service);
                if (host.State != CommunicationState.Opened)
                    host.Open();
                else
                    return false;
            }
            catch (Exception)
            {
                return false;
            }
            return true;
            

        }
        public bool ServiceStop()
        {
            try
            {
                if (host.State == CommunicationState.Opened)
                    host.Close();
                else
                    return false;
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
    }
}
