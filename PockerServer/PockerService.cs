﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using Basic;

namespace PockerServer
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "PockerService" in both code and config file together.
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class PockerService : IPockerService
    {
        private object locker = new object();
        private Dictionary<ServiceClient, IPockerServiceCallBack> _serviceClients;//словарь клиентов и их колбеков
        private Controller _controller; //ссылко на контролер
        public PockerService(Controller contoller)//конструктор сервиса (конструктор без параметров отсутствует)
        {
            this._controller = contoller;
            _serviceClients = new Dictionary<ServiceClient, IPockerServiceCallBack>();
            this._controller.table.PropertyChanged += Table_PropertyChanged;
        }

        private void Table_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)//обработка события
        {
            NotifyClients((Table)sender);
        }

        private bool AddUserInController(Guid guid, string login, int passHash)//общие действия для успешного добавления нового клиента
        {
            if (!_controller.AddClientInList(login)) //проверка успешного добавления игрока на контролере
                return false;
            IPockerServiceCallBack callback = OperationContext.Current.GetCallbackChannel<IPockerServiceCallBack>(); //колбек для клиента
            try
            {
                lock (locker)
                {
                    _serviceClients.Add(new ServiceClient { id = guid, Name = login }, callback); //добавление в список на сервисе
                    //Console.WriteLine("Client " + guid.ToString() + " registred");
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool AuthorizationClient(Guid guid, string login, int passHash) //проверка существования клиента и его подключение
        {
            if (!_controller.CheckUserInBase(login, passHash)) //проверка логина и пароля
                return false;
            return AddUserInController(guid, login, passHash);            
        }

        public bool RegisterClient(Guid guid, string login, int passHash) //регистрация нового клиента и его подключение
        {
            if (!_controller.AddNewUserInBase(login, passHash))
                return false;
            return AddUserInController(guid, login, passHash);
        }

        public void SendTable(Guid guid, Table table)//клиент прислал новый стол
        {
            var query = from c in _serviceClients.Keys where c.id == guid select c;//поиск клиента по его Guid
            _controller.TakeTable(query.First().Name, table);
        }
        
        public void UnRegisterClient(Guid guid)//отключение пользователя
        {
            try
            {
                var query = from c in _serviceClients.Keys where c.id == guid select c;
                lock (locker)
                {
                    _serviceClients.Remove(query.First()); //удаляем из списка на сервисе
                    _controller.RemoveClientFromList(query.First().Name); //удаляем из списка в контроллере
                }
            }
            catch (Exception)
            {

                throw;
            }
           //Console.WriteLine("Client " + guid.ToString() + " unregistred");
        }
        public void NotifyClients(Table table) //вызывает в цикле колбек у всех клиентов из списка и передает новый стол
        {
            var query = (from c in _serviceClients select c.Value).ToList();

            Action<IPockerServiceCallBack> action = delegate (IPockerServiceCallBack callback)
                {
                    callback.NewTableNotify(table);
                };

            query.ForEach(action);
        }
    }
    public class ServiceClient//Класс описывающий клиента в пределах сервиса
    {
        
        public Guid id { get; set; }
        public string Name { get; set; }

    }
}
