﻿using System;
using System.ServiceModel;
using Basic;

namespace PockerServer
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IPockerService" in both code and config file together.
    [ServiceContract(CallbackContract = typeof(IPockerServiceCallBack), SessionMode = SessionMode.Required)]
    public interface IPockerService
    {
        [OperationContract(IsOneWay = true)]
        void SendTable(Guid guid, Table table);
        [OperationContract]
        bool RegisterClient(Guid id, string login, int passHash);
        [OperationContract]
        bool AuthorizationClient(Guid id, string login, int passHash);

        [OperationContract(IsOneWay = true)]
        void UnRegisterClient(Guid id);
    }
    public interface IPockerServiceCallBack
    {
        [OperationContract(IsOneWay = true)]
        void NewTableNotify(Table table);
        [OperationContract(IsOneWay = true)]
        void FacemObTable(); // Если у него нет денег
    }
    
}
